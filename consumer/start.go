package consumer

import (
	"fmt"
	"log"
	"net/http"
)

type pageVars struct {
	DateTime string
	Identity string
}

// Serve starts server and serves routes
func Serve() {
	http.HandleFunc("/", Consume)
	fmt.Println("RabbitMQ consumer running on port 8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
