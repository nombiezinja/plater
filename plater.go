package main

import (
	"bitbucket.com/nombiezinja/deepfrier/cmd"
	"bitbucket.com/nombiezinja/deepfrier/database"
	"bitbucket.com/nombiezinja/plater/consumer"
)

func main() {
	cmd.Setenv()
	database.InitDB()
	consumer.Serve()
}
